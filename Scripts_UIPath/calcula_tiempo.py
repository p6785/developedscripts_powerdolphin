#Script to implement the delay on Python instance, because UIPath
#is limited to a difference of 1s between values

from time import sleep

def detiene_programa(tiempo):
    sleep(tiempo)
    return True

if __name__=="__main__":
    print("inicio de la espera")
    detiene_programa(1) #only for testing
    print("fin de la espera")

