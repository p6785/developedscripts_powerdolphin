import csv, cmath, time, serial

port   = serial.Serial(port = 'COM8', # Test default COM8, modify if necessary
                         baudrate = 9600,
                         bytesize = serial.EIGHTBITS,
                         parity   = serial.PARITY_NONE,
                         stopbits = serial.STOPBITS_ONE)

# Serial behavior
      # A = avance vmax
      # B = avance v1
      # C = avance v2
      # D = avance v3
      # E = avance v4
      # F = avance v5
      # G = detiene avance
      # I = giro izq
      # J = giro der
      # K = detiene giro

def serialTransmit(charInput):
    try:
        port.write(charInput.encode())
    except serial.SerialException:
        print('Port is not available') 
    print(charInput)


def importCSV():
    with open('path_ida3.csv') as File:
        points = []
        reader = csv.reader(File)
        
        for row in reader:
            # The map is done like [row, column] so [y,x]
            y=float(row[0])
            x=float(row[1])
            points.append(complex(x,y))
        nPoints=len(points)  
    return nPoints, points

def turn(pointNew,point,pointOld):
    omegaTurn = 0.5 #rad/s

    dzN = pointNew - point
    dz = point - pointOld
    dTheta = cmath.phase(dzN/dz)
    counterClockwise = (dTheta > 0)
    dtTurn = abs(dTheta)/omegaTurn
    
    dtTurn = 1000*dtTurn #Result processed in miliseconds
    if dtTurn > 0:
        if counterClockwise:
            return ("H", dtTurn) # Left turn
        else:
            return ("L", dtTurn) # Right turn
    else:
        return ("E", dtTurn) # No turn

def goAhead(pointNew,point):
    speed = 2 # m/s
    
    dzN = pointNew-point
    dtFwd = abs(dzN)/speed

    return ("K", dtFwd*1000) #It only returns the goAhead time (given in miliseconds), if it's 0 there isn't


# Create actions table
nPoints, points = importCSV()
actions = []
for i in range(nPoints-1):
    if i == 0:
        actions.append( turn(points[i+1],points[i],2*points[i]-points[i+1]) )
    else:
        actions.append( turn(points[i+1],points[i],points[i-1]) )
    
    actions.append( goAhead(points[i+1],points[i]) )

# Command table adapted to serial connection
order = {
            "H" : {"on" : 'I', "off" : 'K'}, # Left turn
            "K" : {"on" : 'A', "off" : 'G'}, # Go forward
            "L" : {"on" : 'J', "off" : 'K'}  # Right turnj
}
for (action, dt) in actions:
    if dt>0:
        serialTransmit(order.get(action,{}).get("on",None))
        time.sleep(dt/1000)
        serialTransmit(order.get(action,{}).get("off",None))
port.close()

