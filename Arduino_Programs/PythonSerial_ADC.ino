// Transmitter program rewritten to use only a uC, because of EMI on 433 MHz modules
// Copy of the main structure

// Version to be used with a DAC microcontroller or a separated DAC, but not working with
// the analogWrite function, which uses the PWM to create the desired output signal
// NEEDS to change the location of the DAC pins on the uC, please review the code before application


const int VMAX = 0;
const int V1 = 80; // max 255 (8 bits)
const int V2 = 60; // but NOT USABLE (255 = 5V, max remote tension = 3.27V)
const int V3 = 50;
const int V4 = 40;
const int V5 = 20;
const int V_DET = 80; // 80 is the value to reach 1.52V

const int L_TURN = 173; // 173 is the value to reach 3.27V
const int R_TURN = 0;
const int noTurn = 80;

const int PWM_A = 3; // advance output
const int PWM_R = 5; // rotation output

void setup()
{
    Serial.begin(9600); //9600bps 8 bits no parity 1 stop bit (default)
    analogWrite(PWM_R, noTurn);
    analogWrite(PWM_A, V_DET); //no rotation and no speed
}

void loop()
{
    while (Serial.available() > 0) 
    {
      char dato[1];
      dato[0] = Serial.read(); // Reads the transmitted character from Python

      // A = avance vmax
      // B = avance v1
      // C = avance v2
      // D = avance v3
      // E = avance v4
      // F = avance v5
      // G = detiene avance
      // I = giro izq
      // J = giro der
      // K = detiene giro

        if(dato[0]=='A') // A = avance vmax
        {
           analogWrite(PWM_A, VMAX);
        }
        else if(dato[0]=='B') // B = avance v1
        {
           analogWrite(PWM_A, V1);
        }
        else if(dato[0]=='C') // C = avance v2
        {
           analogWrite(PWM_A, V2);
        }
        else if(dato[0]=='D') // D = avance v3
        {
           analogWrite(PWM_A, V3);
        }
        else if(dato[0]=='E') // E = avance v4
        {
           analogWrite(PWM_A, V4);
        }
         else if(dato[0]=='F') // F = avance v5
        {
           analogWrite(PWM_A, V5);
           //digitalWrite(LED_PLACA, LOW);
           //digitalWrite(15, LOW);
        }
        else if(dato[0]=='G') // G = detiene avance
        {
           analogWrite(PWM_A, V_DET);
           //digitalWrite(LED_PLACA, HIGH);
           //digitalWrite(15, HIGH);
        }
        else if(dato[0]=='I') // I = giro izq
        {
           analogWrite(PWM_R, L_TURN);    
        }
        else if(dato[0]=='J') // J = giro der
        {
           analogWrite(PWM_R, R_TURN);    
        }
        else if(dato[0]=='K') // K = detiene giro
        {
           analogWrite(PWM_R, noTurn);    
        }  
    }
}
