// Transmitter program rewritten to use only a uC, because of EMI on 433 MHz modules
// Reprogramed version of the DAC first version, using votage dividers instead of 
// the DAC ports.

const int advancePin = 3;
const int noRotationPin = 5;
const int leftRotationPin = 7;


void setup()
{
    Serial.begin(9600); //9600bps 8 bits no parity 1 stop bit (default)
    pinMode(advancePin, OUTPUT);
    pinMode(noRotationPin, OUTPUT);
    pinMode(leftRotationPin, OUTPUT); // D5 and D7 are EXCLUSIONARY, D5 MUST NOT BE on HIGH while D7 on HIGH
    // D5 on HIGH powers up 1.52V on rotation (no rotation)
    // D7 on HIGH powers up 3.3V on rotation (left rotation), LOW 0V (rotation right)
    // D3 on HIGH powers up 1.52V on speed (no advance), LOW 0V (max speed)
    // A diode has been connected between the D5 output and the D7 output
    // to be used when left rotation output is selected as input
    // Now, D7 at HIGH will have 3.3V on the output thanks to the diode
}

void loop()
{
    while (Serial.available() > 0) 
    {
      char dato[1];
      dato[0] = Serial.read(); // Reads the transmitted character from Python

      // A = advance vmax
      // B = advance v1 -- variable speeds will not be used due to not having the DAC
      // C = advance v2
      // D = advance v3
      // E = advance v4
      // F = advance v5
      // G = stops advance
      // I = left turn
      // J = right turn
      // K = stops turning

        if(dato[0]=='A') // A = advance vmax
        {
           digitalWrite(advancePin, LOW);
        }
        else if(dato[0]=='G') // G = stops advance
        {
           digitalWrite(advancePin, HIGH);
        }
        else if(dato[0]=='I') // I = left turn
        {
           pinMode(leftRotationPin, OUTPUT); // restores the output state
           digitalWrite(leftRotationPin, HIGH); // 3.3V out for left rotation
           digitalWrite(noRotationPin, LOW); // only for power saving, around 1mA
               
        }
        else if(dato[0]=='J') // J = right turn
        {
           digitalWrite(noRotationPin, LOW); // to force 0V
           pinMode(leftRotationPin, OUTPUT); // restores the output state, modified on stopping
           digitalWrite(leftRotationPin, LOW); // both pins at 0V
        }
        else if(dato[0]=='K') // K = stops rotation (D5 must be high, 1.52V)
        {
           pinMode(leftRotationPin, INPUT); // high Z port
           digitalWrite(noRotationPin, HIGH);    
        }  
    }
}
