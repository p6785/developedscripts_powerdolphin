#!/usr/bin/env python
#import rospy
#from turtlesim.msg import Pose
#from nav_msgs.msg import Odometry
import tf.transformations
import numpy as np
import matplotlib.pyplot as plt
import cv2
import sys
import math
from math import pow, atan2, sqrt
from sympy import Symbol
#from trabajo.srv import *
import csv

plotting = False



'''def add_client(xo, yo, tol, v):
     rospy.wait_for_service('goto')
     try:
	 
         add_two_intsg = rospy.ServiceProxy('goto', GoTo)
	 
         resp1 = add_two_intsg(xo, yo, tol, v)#resp1 es true cuando termina el movimientp
	 
         return resp1.success
     except rospy.ServiceException, e:
         print "Service call failed: %s"%e'''

def euclidean_distance(pos_final, pos_ini):
        
        return sqrt(pow((pos_final[0] - pos_ini[0]), 2) +
                    pow(( pos_final[1] - pos_ini[1]), 2))

class camp_pot_virtual:
    """
   laberinto : mapa en entero
   pos_final : punto objetivo final
   inicio : Nodo que contiene el punto inicial
   fin : Nodo que contiene el punto final
   
   
   """
    def __init__(self, mapa, inicio, final):
        self.laberinto = mapa
        self.pos_final = final
        print final
        self.inicio = Nodo(final,inicio,None)
        self.fin = Nodo(final,final,None)
        P = [] #lista de posiciones de los nodos por los que recorrera robot, el camino
        cams = []
        
        
        objetos = []
        self.dp_goal = 1 # El valor de dp_goal determina la forma de atraccion del campo potencial, de tal manera que entre d goal y la posicion final un punto de control es atraido de forma cuadratica. Entre dp_goal yel infinito, la atraccion del punto de control a la meta es de forma conica
        
        # Condicion de parada (para evitar bucle infinito)
        self.iteracciones = 0
        self.max_iter = 20
        P.append(self.inicio.posicion)#Inicializar la posicion del robot

        #en un primer lugar se va ahacer off line , se le va a atribuir un potencial a cada casilla de la cuadricula del mapa
        #print len(mapa[0]) #numero columnas
        #print len(mapa) #numero filas



        #inicializo matriz campo
        campo = []
        for i in range(len(mapa)):
            campo.append([0]*len(mapa[0]))
    

        #busco los nodos objetos y guardo en una lista estos
        # #     
        #
        for i in range(len(mapa)):#filas
            for j in range(len(mapa[0])):#columnas
                #self.actual = [i,j]
                if mapa[i][j] == 1:
                    objetos.append(Nodo(final,[i,j],None))
                    
                else:
                    pass


        #asigno potencial a cada celda 
        for i in range(len(mapa)):#filas
            for j in range(len(mapa[0])):#columnas
                self.actual = [i,j]
                if mapa[i][j] == 1:
                    
                    campo[i][j] = 'obj'
                else:
                    campo[i][j] = round(self.atractiva() + self.repulsiva(objetos),3) #campo almacena valor campo potencial en cada punto

            print campo[i]      
          
              


            
        #print campo[12][2]
        #para hallar recorrido se va seleccionando la casilla con potencial mas bajo de la que se encuentre cerca el robot y esa sera a la que se dirija, asi hasta llegar a la meta
        #-----------------------------------------------------------------------------
        proxima = P[-1]
        
        print '--'
        while self.objetivo(proxima, campo):#Hasta llegar al punto destino o si no existe cambio de la posicion.

            actual = P[-1] #Almacenar la posicion actual;
            #print 'actial'+str(actual)
            self.nodos = self.vecinas(actual) 
            #print 'vecinos'+str(self.nodos)
           
            estudio = campo[actual[0]][actual[1]]
            for i in range(len(self.nodos)): #se recorre la lista de nodos vecinos analizando cada uno
                Nodoe = self.nodos[i]
                #print str(campo[Nodoe[0]][Nodoe[1]]) + '' + str(Nodoe)
                if campo[Nodoe[0]][Nodoe[1]] < estudio:
                    proxima = Nodoe
                    estudio = campo[Nodoe[0]][Nodoe[1]]
               
           
            P.append(proxima)
            cams.append(campo[proxima[0]][proxima[1]])



        print cams
        self.camino = P #Establecer la trayectoria final como la coleccion de todos las posiciones calculadas
      

    def vecinas(self,actual):
        vecinos = []
        """ Para cada celda vecina:
        Si   celda esta ocupada en el mapa, se ignora. No puede formar parte del camino.
        """
        #En el mapa hay un 1 para marcar pared
        try:
            if self.laberinto[actual[0]+1][actual[1]] != 1:#arriba  # and not self.exists(Nodo(self.pos_final,[actual[0]+1, actual[1]],actual), cerrada):
                vecinos.append([actual[0]+1, actual[1]])
        except IndexError,e:
            pass #pasa, no hace nada
            """ IndexErrores una de las excepciones mas basicas y comunes que se encuentran en Python, ya que se genera cada vez que se intenta acceder a un indice que esta fuera de los limites de a list."""
        try:
            if self.laberinto[actual[0]-1][actual[1]] != 1:#abajo # and not self.exists( Nodo(self.pos_final,[actual[0]-1, actual[1]],actual), cerrada):
                vecinos.append([actual[0]-1, actual[1]])
        except IndexError,e:
            pass
        try:
            if self.laberinto[actual[0]][actual[1]-1] != 1:#izquierda# and not self.exists(Nodo(self.pos_final,[actual[0], actual[1]-1],actual), cerrada):
                vecinos.append([actual[0], actual[1]-1])
        except IndexError,e:
            pass
        try:
            if self.laberinto[actual[0]][actual[1]+1] != 1:#derecha# and not self.exists(Nodo(self.pos_final,[actual[0], actual[1]+1],actual), cerrada):
                vecinos.append([actual[0], actual[1]+1])
        except IndexError, e:
            pass
        #diagonales---------------------------------------------------
        try:
            if self.laberinto[actual[0]+1][actual[1]+1] != 1:#arriba derecha  # and not self.exists(Nodo(self.pos_final,[actual[0]+1, actual[1]],actual), cerrada):
                vecinos.append([actual[0]+1, actual[1]+1])
        except IndexError,e:
            pass #pasa, no hace nada
            """ IndexErrores una de las excepciones mas basicas y comunes que se encuentran en Python, ya que se genera cada vez que se intenta acceder a un indice que esta fuera de los limites de a list."""
        try:
            if self.laberinto[actual[0]-1][actual[1]+1] != 1:#abajo derecha# and not self.exists( Nodo(self.pos_final,[actual[0]-1, actual[1]],actual), cerrada):
                vecinos.append([actual[0]-1, actual[1]+1])
        except IndexError,e:
            pass
        try:
            if self.laberinto[actual[0]-1][actual[1]-1] != 1:#abajo izquierda# and not self.exists(Nodo(self.pos_final,[actual[0], actual[1]-1],actual), cerrada):
                vecinos.append([actual[0]-1, actual[1]-1])
        except IndexError,e:
            pass
        try:
            if self.laberinto[actual[0]+1][actual[1]-1] != 1:#arriba izquierda# and not self.exists(Nodo(self.pos_final,[actual[0], actual[1]+1],actual), cerrada):
                vecinos.append([actual[0]+1, actual[1]-1])
        except IndexError, e:
            pass
   

        return vecinos

    def repulsiva(self, objetos):
        pr = 0
        x= self.actual[1]
        y=self.actual[0]
        
        l=1.0
        nu = 1 #k_obs #Siendo nu quien controla de forma relativa la influencia del campo repulsivo para un punto de control en particular
        
        
        for k in range(len(objetos)): #se tiene que sumar el que produce cada objeto
            [y0, x0]=objetos[k].posicion

            di = euclidean_distance(self.actual,objetos[k].posicion) # ro #distancia del robot al objeto
            
            
            ro0 = 2
            if  di <= ro0:
                #pri = nu*pow(1/di - 1/ro0, 2)*(1/pow(di,2))*der_dxy
                pri = 0.5 * nu * pow(1/di - 1/ro0, 2)

            else:
                pri = 0


            pri = nu/di

            pr = pr + pri
            #if pri > pr:
            #    pr= pri


        return pr


    def atractiva(self):
        
        d_obj = euclidean_distance(self.actual, self.pos_final)#distancia robot a objetivo
        C = 1 #k_obj 
         
        x_xg = [self.actual[0]-self.pos_final[0],self.actual[1]-self.pos_final[1]]
        Uatt = 0.5* C * (x_xg[0] * x_xg[0] + x_xg[1] * x_xg[1] )
        
        Uatt = C * euclidean_distance(self.pos_final,self.actual)

        return Uatt


   
    def objetivo(self,proxima, campo):
        self.iteracciones += 1 
        
        if (self.fin.posicion == proxima) or (round(campo[proxima[0]][proxima[1]]) == 0): 
            #if round(F) == 0: #Se busca el actual final en lista abierta . Si posicion de actual final es igual a posicion de uno de los nodos almacenados en lista abierta
            #objetivo = proxima# se guarda en 'objetivo' el actual final
            #print len(abierta)
            #self.camino = self.camino(objetivo) 
            return False # al poner return devuelve False y sale de la funcion sin ejecutar el resto 
        if self.iteracciones > self.max_iter:
            print("Problema al calcular la trayectoria")
            #self.camino = [self.inicio.posicion] 
            return False
        return True

class Nodo:
    """
   pos_final : punto objetivo
   posicion : localizacion x,y del actual en la cuadricula
   padre : Nodo padre
   
   """
    def __init__(self,pos_final,posicion=[0,0],padre=None):
        self.posicion = posicion
        self.padre = padre
        
        
        



    



class Planner:

    def __init__(self):
        
        # A subscriber to the topic '/odom'. self.update_pose is called
        # when a message of type Pose is received.
        '''self.pose_subscriber = rospy.Subscriber('/odom',
                                                Odometry, self.update_pose)

        self.pose = Pose()'''

        # Load map
        self.map = np.genfromtxt('/home/isabel/catkin_ws/src/trabajo2/worlds/map.csv', delimiter=',')
        self.height = self.map.shape[0]
        self.width = self.map.shape[1]
        self.resolution = 1
        
        # Print map
        image = np.flipud(self.map)
        #plt.figure()
        #plt.imshow(image, cmap=plt.get_cmap('binary'))
        #plt.show()

        
    '''def update_pose(self, data):
        """Callback function which is called when a new message of type Pose is
        received by the subscriber."""
        
        self.pose.x = round(data.pose.pose.position.x, 4)
        self.pose.y = round(data.pose.pose.position.y, 4)
        #print self.pose.x
        orientation = [data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w]
        (_,_,yaw) = tf.transformations.euler_from_quaternion(orientation)
        
        self.pose.theta = yaw'''

    

        



    def compute_path(self, start_cell, goal_cell): #esta funcion crea el camino a seguir y  pinta linea desde inicio a fin
        """Compute path."""
        path = [] #donde se guardan coordenadas del camino

        #path.append(start_cell)
        
        
        ######################
        # TODO: Implement A* #
        ######################
        inicio = start_cell
        final = goal_cell
        mapa = self.map
        mapa = mapa.astype(int) #convertir mapa a tipo entero
        #print mapa[0][0]
        
        algoritmo = camp_pot_virtual(mapa, inicio, final)
        #print algoritmo.camino[0]
        
       
        print '______path:________'
       
        
        path = algoritmo.camino
        print path
        
        myFile=open('path_2.csv','w')
       

        with myFile:
            writer=csv.writer(myFile)
            writer.writerows(path)
        print("Writting complete")
        ######################
        # End A*             #
        ######################
        #path.append(goal_cell)
        # Print path
        xp = []
        yp = []
        
        for point in path:
            xp.append(point[1])
            yp.append(point[0])
        
        image = np.flipud(self.map)
        plt.figure()
        plt.imshow(image, cmap=plt.get_cmap('binary'))
        plt.plot(xp,yp,'ro-', linewidth=2, markersize=5)
        plt.show()
        #print yp
        #print xp



        
        return path

    
 
    
    def goto(self, current_cell):
        """Moves the robot to the goal."""

       # goal_pose = Pose()
        # Get the input from the user.
        # Test with -0.5,3.5 meters
        goal_pose_x = input("Set your x goal: ")
        goal_pose_y = input("Set your y goal: ")


        yg=0
        xg=0
        
        # Compute current and goal cell
        # TODO: compute automatically .  Hay que hacer un cambio de sistema de referencia de los puntos. Lo vemos en un sistema de referencia hay que cambiarlo al de la cuadricula (esquina izq arriba con eje y invertido)
        ys = int(goal_pose_y)
        xs = int(goal_pose_x)
        
        if ys>=0:
            yg = -ys + 8
        else:
            yg = -ys + 8
            yg = abs(yg)


        xg = xs + 11
        
        #current_cell = [12,2] 

        goal_cell = [ys, xs]
        #goal_cell = [yg-1,xg-1]
        
        
        print 'goal_cell: '+str(goal_cell)
        path = self.compute_path(current_cell,goal_cell)
        #path =[[12, 2]]
        #print path #[[12, 2], [4, 9]]
        pro_cell = path[-1]
        print "--------------"
        '''v=100
        for point in path:
            # TODO: Call service GoTo.        Hay que hacer un cambio sistema de referencia para el go to goal (no coincide con cuadricula, al menos en la version kinetic no coincide)
            tol = 0.1
            
            xc = point[1]
          
            yc = point[0]
           
            
            xo = xc-2
           
            yo = -(yc-12)
           

            plotting = add_client(xo, yo, tol,v) #llamada al sevicio de mover robot
            '''
        return pro_cell
            
        
if __name__ == '__main__':

    x = Planner()
    current_cell = [12,2]
    while(1):
        pro = x.goto(current_cell)
        current_cell = pro


    '''try:
        rospy.init_node('robot_planner', anonymous=True)

        x = Planner()
        current_cell = [12,2]
        while not rospy.is_shutdown():
            pro = x.goto(current_cell)
            current_cell = pro
            
        rospy.spin()
    except rospy.ROSInterruptException:
        pass'''
